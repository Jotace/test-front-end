import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";

import PaginaInicio from './components/PaginaInicio';
import PaginaProducto from './components/PaginaProducto';

export default function App() {
  return (
    <Switch>
      <Route exact path="/" component={PaginaInicio} />
      <Route path="/:id" component={PaginaProducto} />
    </Switch>
  );
}

export function actualizarCarrito() {
  let cantidadCarrito = document.body.querySelector('#carrito');
  let localCarrito = localStorage.getItem('carritoItems');
  if(localCarrito && localCarrito != '') {
    cantidadCarrito.innerHTML = localCarrito;
  } else {
    cantidadCarrito.innerHTML = '0';
  }
}