import React from 'react';
import { actualizarCarrito } from '../App';

class Header extends React.Component {
  render() {
    let localCartItems = localStorage.getItem('carritoItems');
    let productosCarrito = '0';
    if(localCartItems && localCartItems != '') {
      productosCarrito = localCartItems;
    } else {
      productosCarrito = '0';
    }
    return (
      <header className="App-header">
        <nav className="navbar navbar-expand-xl navbar-light py-3 bg-secondary w-100">
          <div className="container-fluid">
            <a className="font-weight-bold text-decoration-none text-white display-6" href="/">Front End Test</a>
            <div className="text-white">Carrito: <span id="carrito">{productosCarrito}</span> productos</div>
          </div>
        </nav>
        <nav aria-label="breadcrumb" className="p-3 w-100">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active"><a href="/">Inicio</a></li>
          </ol>
        </nav>
      </header>
    );
  }
}

export default Header;