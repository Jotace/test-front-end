import React from 'react';
import Image from './Image';

class Item extends React.Component {
  render() {
    return (
      <div className="product-card col-12 col-sm-6 col-md-4 col-lg-3 text-center position-relative mb-5" id={this.props.itemInfo.id}>
        <div className="p-3 p-md-4 shadow">
          <Image className="img-fluid" imgUrl={this.props.itemInfo.imgUrl}/>
          <a className="text-decoration-none stretched-link d-flex justify-content-between fs-1-2 pt-3" href={this.props.itemInfo.id}>
            <div className="">{this.props.itemInfo.brand}</div>
            <div className="">{this.props.itemInfo.model}</div>
          </a>
          <div className="text-danger fs-2 d-flex justify-content-end">{this.props.itemInfo.price}€</div>
        </div>
      </div>
    );
  } 
}

export default Item;