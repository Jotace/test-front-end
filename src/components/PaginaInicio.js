import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Header from './Header';
import Search from './Search';
import Item from './Item';

class PaginaInicio extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      DataisLoaded: false
    };
  }
  componentDidMount() {
    fetch("https://front-test-api.herokuapp.com/api/product")
    .then((res) => res.json())
    .then((json) => {
      this.setState({
        items: json,
        DataisLoaded: true
      });
    });
  }
  
  render() {
    const { DataisLoaded, items } = this.state;
    if (!DataisLoaded) {
      return <div>Cargando...</div>;
    } else{
      return (
        <div className="app">
          <Header />

          <Search />

          <div className="row">
            {
              items.map(function(item) {
                return <Item itemInfo={item} key={item.id}/>
              })
            }
          </div>
        </div>
      );
    }
  }
}
  
  export default PaginaInicio;
  