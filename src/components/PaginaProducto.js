import React, { useState, useEffect } from 'react';
import Header from './Header';
import { useParams } from "react-router-dom";
import { actualizarCarrito } from '../App';


export default function PaginaProducto(props){
  const estadoInicial = {
    producto: {},
    cargando: true,
  };
  const [producto, setProducto] = useState(estadoInicial);
  const { id } = props.match.params;

  useEffect(() => {
    const getProducto = async () => {
      fetch(
        "https://front-test-api.herokuapp.com/api/product/" + id)
          .then((res) => res.json())
          .then((json) => {
            setProducto(json);
            console.log(json);
      });

    }

    getProducto();
  }, []);

  return producto.cargando ? (
    <div>Cargando...</div>
  ) : (
    <div className="app">
      <Header />
      <div className="row mt-4 mb-5">

        <div className="col-12 text-center">
          <h1>{producto.brand + ' ' + producto.model}</h1>
        </div>
      </div>

      <div className="row align-items-center pb-5">
        <div className="col-12 col-md-6 text-center">
          <img src={producto.imgUrl}/>
        </div>

        <div className="col-12 col-md-6 mt-4 mt-md-0 text-center text-md-start">
          <h2>Descripción</h2>
          <ul className="lista-caracteristicas">
            <li>Marca: {producto.brand}</li> 
            <li>Modelo: {producto.model}</li>
            <li>Precio: {producto.price} €</li>
            <li>CPU: {producto.cpu}</li>
            <li>RAM: {producto.ram}</li>
            <li>SO: {producto.os}</li>
            <li>Resolución pantalla: {producto.displayResolution}</li>
            <li>Batería: {producto.battery}</li>
            <li>Cámaras: 
              <ul>
                <li>{producto.primaryCamera}</li>
                <li>{producto.secondaryCmera}</li>
              </ul>
            </li>
            <li>Dimensiones: {producto.dimentions}</li>
            <li>Peso: {producto.weight}</li>
          </ul>

          <div className="d-flex justify-content-start mt-3">
            <select name="select" className="almacenamiento" name="almacenamiento">
              <option>Elige memoria</option>
              {
                producto.internalMemory.map((value, i) => {
                  return (<option key={value} value={i}>{value}</option>);
                })
              }
            </select>

            <select name="select" className="colores ms-3" name="colores">
              <option>Elige color</option>
              {
                producto.colors.map((value, i) => {
                  return (<option key={value} value={i}>{value}</option>);
                })
              }
            </select>
          </div>
          <button className="btn btn-secondary mt-3" onClick={actualizarCarrito}>Añadir al carrito</button>
        </div>

      </div>
      
    </div>
  )

  function addToCart() {
    let idProducto = id;
    let almacenamiento = document.body.querySelector('select[name=almacenamiento]').value;
    let color = document.body.querySelector('select[name=color]').value;
    
    let product = {
      id: idProducto,
      colorCode: color,
      storageCode: almacenamiento,
    }

    fetch(
      "https://front-test-api.herokuapp.com/api/product/" + id)
        .then((res) => res.json())
        .then((json) => {
          setProducto(json);
    });
    (async () => {
      const rawResponse = await fetch('https://front-test-api.herokuapp.com/api/cart', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
      });
      const content = await rawResponse.json();
    
      var localCartItems = localStorage.getItem('carritoItems');
      if(localCartItems && localCartItems != '') {
        localCartItems = parseInt(localCartItems);
        localStorage.setItem('carritoItems', localCartItems + 1);
      } else {
        localStorage.setItem('carritoItems', '1');
      }

      actualizarCarrito();
    })();
  }
}

