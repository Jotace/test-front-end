import React from 'react';

class Search extends React.Component {
  render() {
    return (
      <div className="d-flex justify-content-end p-3">
        <form className="col-12 col-md-4 pb-4">
          <input className="form-control" type="search" placeholder="Buscar..." aria-label="Buscar" />
        </form>
      </div>
    );
  }
}

export default Search;